package com.gitlab.residwi.spring.library.command.executor;

import com.gitlab.residwi.spring.library.command.Command;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@AutoConfigureMockMvc
@SpringBootTest(classes = DefaultCommandExecutorTest.Application.class)
class DefaultCommandExecutorTest {

    @Autowired
    private CommandExecutor commandExecutor;

    @Test
    void testExecuteSuccess() {
        var result = commandExecutor.execute(MyCommand.class, "World");
        assertEquals("Hello World", result);
    }

    @Test
    void testExecuteFailed() {
        assertThrows(RuntimeException.class, () -> commandExecutor.execute(MyCommand.class, "Failed"));
    }

    @Test
    void testExecuteObjectSuccess() {
        HelloCommandResponse result = commandExecutor.execute(HelloCommand.class, new HelloCommandRequest("World"));

        assertEquals(new HelloCommandResponse("Hello World"), result);
    }

    @Test
    void testValidationError() {
        HelloCommandRequest helloCommandRequest = new HelloCommandRequest();

        assertThrows(ConstraintViolationException.class, () -> commandExecutor.execute(HelloCommand.class, helloCommandRequest));
    }

    public interface MyCommand extends Command<String, String> {
    }

    public interface HelloCommand extends Command<HelloCommandRequest, HelloCommandResponse> {
    }

    @SpringBootApplication
    public static class Application {

        @Bean
        public MyCommand myCommand() {
            return new MyCommandImpl();
        }

        @Bean
        public HelloCommand helloCommand() {
            return new HelloCommandImpl();
        }

    }

    public static class MyCommandImpl implements MyCommand {

        @Override
        public String execute(String request) {
            if (request.equals("Failed")) {
                throw new RuntimeException("Failed Test");
            }

            return String.format("Hello %s", request);
        }
    }

    public static class HelloCommandImpl implements HelloCommand {

        @Override
        public HelloCommandResponse execute(HelloCommandRequest request) {
            return new HelloCommandResponse(String.format("Hello %s", request.getName()));
        }
    }

    private static class HelloCommandRequest {

        @NotBlank
        private String name;

        public HelloCommandRequest() {
        }

        public HelloCommandRequest(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private static class HelloCommandResponse {

        private String response;

        public HelloCommandResponse(String response) {
            this.response = response;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            HelloCommandResponse that = (HelloCommandResponse) o;

            return response.equals(that.response);
        }

        @Override
        public int hashCode() {
            return response.hashCode();
        }
    }
}
