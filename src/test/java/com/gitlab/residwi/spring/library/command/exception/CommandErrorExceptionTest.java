package com.gitlab.residwi.spring.library.command.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.command.Command;
import com.gitlab.residwi.spring.library.command.executor.CommandExecutor;
import com.gitlab.residwi.spring.library.common.exception.CommonErrorException;
import com.gitlab.residwi.spring.library.common.helper.ResponseHelper;
import com.gitlab.residwi.spring.library.common.model.response.ResponsePayload;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.constraints.NotBlank;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = CommandErrorExceptionTest.Application.class)
@AutoConfigureMockMvc
class CommandErrorExceptionTest {

    private final static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testValidationError() throws Exception {
        mockMvc.perform(post("/hello")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(new HelloCommandRequest())))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.code").value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath("$.status").value(HttpStatus.BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors.name").value("NotBlank"));
    }

    public interface HelloCommand extends Command<HelloCommandRequest, String> {

    }

    @SpringBootApplication
    public static class Application {

        @Bean
        public HelloCommand helloCommand() {
            return new HelloCommandImpl();
        }

        @RestControllerAdvice
        public static class ErrorController implements MessageSourceAware, CommonErrorException {

            private static final Logger log = LoggerFactory.getLogger(ErrorController.class);

            private MessageSource messageSource;

            @Override
            public MessageSource getMessageSource() {
                return messageSource;
            }

            @Override
            public void setMessageSource(MessageSource messageSource) {
                this.messageSource = messageSource;
            }

            @Override
            public Logger getLogger() {
                return log;
            }
        }

        @RestController
        public static class HelloController {

            @Autowired
            private CommandExecutor commandExecutor;

            @PostMapping(value = "/hello", consumes = MediaType.APPLICATION_JSON_VALUE)
            public ResponsePayload<String> hello(@RequestBody HelloCommandRequest request) {
                commandExecutor.execute(HelloCommand.class, request);
                return ResponseHelper.ok();

            }
        }
    }

    public static class HelloCommandImpl implements HelloCommand {

        @Override
        public String execute(HelloCommandRequest request) {
            return String.format("Hello %s", request);
        }
    }

    public static class HelloCommandRequest {

        @NotBlank(message = "NotBlank")
        private String name;

        public String getName() {
            return name;
        }
    }
}
