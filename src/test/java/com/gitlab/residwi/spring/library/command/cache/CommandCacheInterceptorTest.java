package com.gitlab.residwi.spring.library.command.cache;

import com.gitlab.residwi.spring.library.command.Command;
import com.gitlab.residwi.spring.library.command.executor.CommandExecutor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import redis.embedded.RedisServer;

import javax.validation.constraints.NotBlank;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AutoConfigureMockMvc
@SpringBootTest(classes = CommandCacheInterceptorTest.Application.class)
class CommandCacheInterceptorTest {

    @Autowired
    private CommandExecutor commandExecutor;

    private RedisServer redisServer;

    @Autowired
    private HelloCommandImpl helloCommand;

    @BeforeEach
    void setUp() throws IOException {
        redisServer = new RedisServer(6379);
        redisServer.start();
    }

    @AfterEach
    void tearDown() {
        redisServer.stop();
    }

    @Test
    void testEnableCacheInterceptor() {
        for (int i = 0; i < 3; i++) {
            var result = commandExecutor.execute(HelloCommand.class, new HelloCommandRequest("World"));
            assertEquals(new HelloCommandResponse("Hello World"), result);
        }
        assertEquals(1, helloCommand.getCounter());
    }

    public interface HelloCommand extends Command<HelloCommandRequest, HelloCommandResponse> {
    }

    @SpringBootApplication
    @PropertySource("classpath:cache.properties")
    public static class Application {

        @Bean
        public HelloCommand helloCommand() {
            return new HelloCommandImpl();
        }

    }

    public static class HelloCommandImpl implements HelloCommand {

        private int counter = 0;

        public int getCounter() {
            return counter;
        }

        @Override
        public HelloCommandResponse execute(HelloCommandRequest request) {
            counter++;
            return new HelloCommandResponse(String.format("Hello %s", request.getName()));
        }

        @Override
        public String cacheKey(HelloCommandRequest request) {
            return request.getName();
        }

        @Override
        public Class<HelloCommandResponse> responseClass() {
            return HelloCommandResponse.class;
        }
    }


    private static class HelloCommandRequest {

        @NotBlank
        private String name;

        public HelloCommandRequest(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private static class HelloCommandResponse {

        private String response;

        public HelloCommandResponse() {
        }

        public HelloCommandResponse(String response) {
            this.response = response;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            HelloCommandResponse that = (HelloCommandResponse) o;

            return response.equals(that.response);
        }

        @Override
        public int hashCode() {
            return response.hashCode();
        }
    }
}
