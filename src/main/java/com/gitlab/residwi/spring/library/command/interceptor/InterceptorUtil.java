package com.gitlab.residwi.spring.library.command.interceptor;

import com.gitlab.residwi.spring.library.command.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.OrderComparator;

import java.util.*;
import java.util.stream.Collectors;

public final class InterceptorUtil {

    private static final Logger log = LoggerFactory.getLogger(InterceptorUtil.class);

    private InterceptorUtil() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static List<CommandInterceptor> getCommandInterceptors(ApplicationContext applicationContext) {
        Map<String, CommandInterceptor> beans = applicationContext.getBeansOfType(CommandInterceptor.class);
        return beans.values().stream()
                .sorted(OrderComparator.INSTANCE)
                .collect(Collectors.toList());
    }

    public static <R, T> Optional<T> fireBefore(List<CommandInterceptor> commandInterceptors, Command<R, T> command, R request) {
        return getListOfBeforeExecute(commandInterceptors, command, request)
                .stream()
                .filter(Objects::nonNull)
                .findFirst();
    }

    private static <R, T> List<T> getListOfBeforeExecute(List<CommandInterceptor> commandInterceptors, Command<R, T> command, R request) {
        return commandInterceptors
                .stream()
                .map(commandInterceptor -> doBeforeExecuteWithFallback(command, request, commandInterceptor))
                .collect(Collectors.toList());
    }

    private static <R, T> T doBeforeExecuteWithFallback(Command<R, T> command, R request, CommandInterceptor commandInterceptor) {
        try {
            return commandInterceptor.before(command, request);
        } catch (Throwable throwable) {
            log.warn("Error beforeExecute() interceptor " + commandInterceptor.getClass().getName(), throwable);
        }
        return null;
    }

    public static <R, T> void fireAfterSuccess(List<CommandInterceptor> commandInterceptors, Command<R, T> command, R request, T response) {
        getListOfAfterSuccessExecute(commandInterceptors, command, request, response);
    }

    private static <R, T> void getListOfAfterSuccessExecute(Collection<CommandInterceptor> commandInterceptors, Command<R, T> command, R request, T response) {
        commandInterceptors.forEach(
                commandInterceptor -> doAfterSuccessExecuteWithFallback(command, request, response, commandInterceptor)
        );
    }

    private static <R, T> void doAfterSuccessExecuteWithFallback(Command<R, T> command, R request, T response, CommandInterceptor commandInterceptor) {
        try {
            commandInterceptor.afterSuccess(command, request, response);
        } catch (Throwable throwable) {
            log.warn("Error afterSuccessExecute() interceptor " + commandInterceptor.getClass().getName(), throwable);
        }
    }

    public static <R, T> void fireAfterFailed(List<CommandInterceptor> commandInterceptors, Command<R, T> command, R request, Throwable throwable) {
        getListOfAfterFailedExecute(commandInterceptors, command, request, throwable);
    }

    private static <R, T> void getListOfAfterFailedExecute(List<CommandInterceptor> commandInterceptors, Command<R, T> command, R request, Throwable throwable) {
        commandInterceptors.forEach(
                commandInterceptor -> doAfterFailedExecuteWithFallback(command, request, throwable, commandInterceptor)
        );
    }

    private static <R, T> void doAfterFailedExecuteWithFallback(Command<R, T> command, R request, Throwable throwable, CommandInterceptor commandInterceptor) {
        try {
            commandInterceptor.afterFailed(command, request, throwable);
        } catch (Throwable e) {
            log.warn("Error afterFailedExecute() interceptor " + commandInterceptor.getClass().getName(), e);
        }
    }
}

