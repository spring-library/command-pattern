package com.gitlab.residwi.spring.library.command.cache;

import com.gitlab.residwi.spring.library.command.Command;
import com.gitlab.residwi.spring.library.command.interceptor.CommandInterceptor;
import com.gitlab.residwi.spring.library.command.properties.CommandProperties;
import com.gitlab.residwi.spring.library.common.helper.JsonHelper;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.time.Duration;
import java.util.Collection;
import java.util.Optional;

public class CommandCacheInterceptor implements CommandInterceptor, Ordered {

    private StringRedisTemplate redisTemplate;

    private CommandProperties commandProperties;

    private JsonHelper jsonHelper;

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void setJsonHelper(JsonHelper jsonHelper) {
        this.jsonHelper = jsonHelper;
    }

    public void setCommandProperties(CommandProperties commandProperties) {
        this.commandProperties = commandProperties;
    }

    @Override
    public <R, T> T before(Command<R, T> command, R request) {
        var jsonValue = Optional.ofNullable(redisGet(command.cacheKey(request)));

        return jsonValue
                .map(json -> readJson(command, json))
                .orElse(null);
    }

    @Override
    public <R, T> void afterSuccess(Command<R, T> command, R request, T response) {
        evictCommandResponse(command, request);
        cacheCommandResponse(command, request, response);
    }

    private <R, T> void cacheCommandResponse(Command<R, T> command, R request, T response) {
        redisSet(command.cacheKey(request), writeJson(response), commandProperties.getCache().getTimeout());
    }

    private <R, T> void evictCommandResponse(Command<R, T> command, R request) {
        redisDelete(command.evictKeys(request));
    }

    private String redisGet(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    private void redisSet(String key, String value, Duration duration) {
        redisTemplate.opsForValue().set(key, value, duration);
    }

    private void redisDelete(Collection<String> keys) {
        redisTemplate.delete(keys);
    }

    private <T> String writeJson(T response) {
        return jsonHelper.toJson(response);
    }

    private <R, T> T readJson(Command<R, T> command, String json) {
        return jsonHelper.fromJson(json, command.responseClass());
    }
}
