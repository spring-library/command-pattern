package com.gitlab.residwi.spring.library.command.executor;

import com.gitlab.residwi.spring.library.command.Command;
import com.gitlab.residwi.spring.library.command.interceptor.CommandInterceptor;
import com.gitlab.residwi.spring.library.command.interceptor.InterceptorUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class DefaultCommandExecutor implements CommandExecutor, ApplicationContextAware, InitializingBean {

    private Validator validator;

    private ApplicationContext applicationContext;

    private List<CommandInterceptor> commandInterceptors;

    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void afterPropertiesSet() {
        commandInterceptors = InterceptorUtil.getCommandInterceptors(applicationContext);
    }

    @Override
    public <R, T> T execute(Class<? extends Command<R, T>> commandClass, R request) {
        Command<R, T> command = applicationContext.getBean(commandClass);
        validateRequestIfNeeded(request, command);
        return doExecute(command, request);
    }

    private <R, T> void validateRequestIfNeeded(R request, Command<R, T> command) {
        if (command.validateRequest()) {
            validateAndThrownIfInvalid(request);
        }
    }

    private <R> void validateAndThrownIfInvalid(R request) throws ConstraintViolationException {
        Set<ConstraintViolation<R>> constraintViolations = validator.validate(request);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
    }

    private <R, T> T doExecute(Command<R, T> command, R request) {
        Optional<T> interceptor = InterceptorUtil.fireBefore(commandInterceptors, command, request);
        return interceptor.orElseGet(() -> doExecuteCommand(request, command));
    }

    private <R, T> T doExecuteCommand(R request, Command<R, T> command) {
        try {
            var response = command.execute(request);
            InterceptorUtil.fireAfterSuccess(commandInterceptors, command, request, response);
            return response;
        } catch (Throwable throwable) {
            InterceptorUtil.fireAfterFailed(commandInterceptors, command, request, throwable);
            return command.fallback(throwable, request);
        }
    }
}
