package com.gitlab.residwi.spring.library.command.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@ConfigurationProperties("library.command")
public class CommandProperties {

    private CacheProperties cache = new CacheProperties();

    public CommandProperties() {
    }

    public CommandProperties(CacheProperties cache) {
        this.cache = cache;
    }

    public CacheProperties getCache() {
        return cache;
    }

    public void setCache(CacheProperties cache) {
        this.cache = cache;
    }

    public static class CacheProperties {

        private boolean enabled = false;

        private Duration timeout = Duration.ofSeconds(10);

        public CacheProperties() {
        }

        public CacheProperties(boolean enabled, Duration timeout) {
            this.enabled = enabled;
            this.timeout = timeout;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public Duration getTimeout() {
            return timeout;
        }

        public void setTimeout(Duration timeout) {
            this.timeout = timeout;
        }
    }
}
