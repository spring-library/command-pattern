package com.gitlab.residwi.spring.library.command.interceptor;

import com.gitlab.residwi.spring.library.command.Command;

public interface CommandInterceptor {

    default <R, T> T before(Command<R, T> command, R request) {
        return null;
    }

    default <R, T> void afterSuccess(Command<R, T> command, R request, T response) {
    }

    default <R, T> void afterFailed(Command<R, T> command, R request, Throwable throwable) {
    }
}
