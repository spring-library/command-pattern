package com.gitlab.residwi.spring.library.command.cache;

import java.util.Collection;
import java.util.Collections;

public interface CommandCacheable<R, T> {

    default String cacheKey(R request) {
        return null;
    }

    default Collection<String> evictKeys(R request) {
        return Collections.emptyList();
    }

    default Class<T> responseClass() {
        throw new UnsupportedOperationException("No response class available.");
    }
}
