package com.gitlab.residwi.spring.library.command.configuration;

import com.gitlab.residwi.spring.library.command.cache.CommandCacheInterceptor;
import com.gitlab.residwi.spring.library.command.properties.CommandProperties;
import com.gitlab.residwi.spring.library.common.helper.JsonHelper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

@Configuration
@ConditionalOnProperty(value = "library.command.cache.enabled", havingValue = "true")
public class CommandCacheAutoConfiguration {

    @Bean
    public CommandCacheInterceptor commandCacheInterceptor(JsonHelper jsonHelper,
                                                           StringRedisTemplate redisTemplate,
                                                           CommandProperties commandProperties) {
        var interceptor = new CommandCacheInterceptor();
        interceptor.setJsonHelper(jsonHelper);
        interceptor.setRedisTemplate(redisTemplate);
        interceptor.setCommandProperties(commandProperties);
        return interceptor;
    }

}
