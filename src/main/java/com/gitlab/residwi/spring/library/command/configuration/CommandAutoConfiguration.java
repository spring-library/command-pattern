package com.gitlab.residwi.spring.library.command.configuration;

import com.gitlab.residwi.spring.library.command.executor.CommandExecutor;
import com.gitlab.residwi.spring.library.command.executor.DefaultCommandExecutor;
import com.gitlab.residwi.spring.library.command.properties.CommandProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validator;

@Configuration
@EnableConfigurationProperties(CommandProperties.class)
public class CommandAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public CommandExecutor commandExecutor(Validator validator) {
        var commandExecutor = new DefaultCommandExecutor();
        commandExecutor.setValidator(validator);
        return commandExecutor;
    }

}
