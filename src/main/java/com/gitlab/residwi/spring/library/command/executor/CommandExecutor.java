package com.gitlab.residwi.spring.library.command.executor;

import com.gitlab.residwi.spring.library.command.Command;

public interface CommandExecutor {

    <R, T> T execute(Class<? extends Command<R, T>> commandClass, R request);
}
