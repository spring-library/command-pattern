package com.gitlab.residwi.spring.library.command.exception;

public class CommandRuntimeException extends RuntimeException {

    public CommandRuntimeException() {
    }

    public CommandRuntimeException(String message) {
        super(message);
    }

    public CommandRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandRuntimeException(Throwable cause) {
        super(cause);
    }
}
