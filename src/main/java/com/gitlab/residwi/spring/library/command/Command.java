package com.gitlab.residwi.spring.library.command;

import com.gitlab.residwi.spring.library.command.cache.CommandCacheable;
import com.gitlab.residwi.spring.library.command.exception.CommandRuntimeException;

public interface Command<R, T> extends CommandCacheable<R, T> {

    T execute(R request);

    default T fallback(Throwable throwable, R request) {
        throw new CommandRuntimeException(throwable);
    }

    default boolean validateRequest() {
        return true;
    }
}
