# Command Pattern Library

This library for simplify Command Pattern as Service Layer.

## Setup Dependency

 ```xml
<dependency>
    <groupId>com.gitlab.residwi</groupId>
    <artifactId>spring-library-command-pattern</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- add repository -->
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/12070658/-/packages/maven</url>
    </repository>
</repositories>
 ```

## Create Command

Command Library need request and response class. We recommend using POJO class, not primitive data type or java collection. To
create command, we can create command using interface `Command<R,T>`.

```java
public interface GetBinListCommand extends Command<GetBinListCommandRequest, GetBinListWebResponse> {

}
```

All logic implementation of the command is in `T execute(R)` method.

```java
@Service
public class GetBinListCommandImpl implements GetBinListCommand {

    @Autowired
    private BinListApiClient binListApiClient;

    @Override
    public GetBinListWebResponse execute(GetBinListCommandRequest request) {
        return toWebResponse(binListApiClient.lookup(request.getNumber()));
    }

    private GetBinListWebResponse toWebResponse(BinResponse response) {
        GetBinListWebResponse webResponse = new GetBinListWebResponse();
        BeanUtils.copyProperties(response, webResponse);
        return webResponse;
    }
}
```

## Execute Command

After we create command, we can use `CommandExecutor` to execute the command.

```java
@RestController
public class BinListController {

    @Autowired
    private CommandExecutor commandExecutor;

    @GetMapping(
            value = "/binlist/{number}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponsePayload<GetBinListWebResponse> lookup(@PathVariable("number") String number) {
        return ResponseHelper.ok(
                commandExecutor.execute(GetBinListCommand.class, toGetBinListCommandRequest(number))
        );
    }
}
```

## Validation

By default, before `execute()` method is executed by `CommandExecutor`. `CommandExecutor` will validate the request
using bean validation.

```java
public class GetBinListCommandRequest {

    @NotBlank(message = "NotBlank")
    @Digits(message = "MustDigit", integer = 8, fraction = 0)
    private String number;

    public GetBinListCommandRequest() {
    }

    public GetBinListCommandRequest(String number) {
        this.number = number;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
```

If we want to skip validation, we can override method `validateRequest` on our `Command`

```java
@Service
public class GetBinListCommandImpl implements GetBinListCommand {

    @Override
    public boolean validateRequest() {
        return false; // skip validation process
    }
}
```

## Command Interceptor

Command Library support command interceptor. With command interceptor we can manipulate data before and after command
executed. We only need to create spring bean of `CommandInterceptor`

```java
public interface CommandInterceptor {

    // executed before execute command
    default <R, T> T before(Command<R, T> command, R request) {
        // empty mean we will execute the command
        // non empty mean we will return the data without execute the command
        return null;
    }

    // executed after success execute command
    default <R, T> void afterSuccess(Command<R, T> command, R request, T response) {
        
    }

    // executed after failed execute command
    default <R, T> void afterFailed(Command<R, T> command, R request, Throwable throwable) {
        
    }
}
```

## Caching

`CommandInterceptor` is powerful feature. We provide command caching using this feature. Caching is feature for cache
command result on redis after finish execute command. With this feature, command will execute one time, and next time
when we execute command again, it will directly return from redis. This can make slow command process become faster.

```java
@Service
public class GetBinListCommandImpl implements GetBinListCommand {

    @Override
    public String cacheKey(GetBinListCommandRequest request) {
        return request.getNumber(); // return cache key for redis
    }

    @Override
    public Class<GetBinListWebResponse> responseClass() {
        return GetBinListWebResponse.class; // return class for json mapper 
    }
}
```

Command Library will cache the data using `StringRedisTemplate`. So we need to make sure we have
`StringRedisTemplate` bean on our spring application.

By default, caching is disabled. You need to enabled it using configuration. We can also change timeout cache from
configuration properties.

```properties
library.command.cache.enabled=true
library.command.cache.timeout=10m
``` 